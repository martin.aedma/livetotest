
import { EventsList } from './EventsList';
import { EventView } from './EventView';

function App() {
  return (
    <div>  
        {EventView()}
        {EventsList()}             
    </div>
  );
}
export default App;




