import React from "react";
import { useDispatch, useSelector } from "react-redux";
import 'bootstrap/dist/css/bootstrap.min.css';
import {toggleEventView} from '../slices/apiSlice';


const togglePriceVisibility = (ind) => {
    const element = document.getElementById(ind.index);
    if (element.style.display === 'none') {
        element.style.display = 'block'
    } else {
        element.style.display = 'none'
    }
}

const createDateFromString = (dateString) => {
    const monthYear = dateString.split('-');
    const day = monthYear[2].split('T');
    return new Date(monthYear[1] + '-' + day[0] + '-' + monthYear[0]);
}

const calculateEventStatus  = (currentDate, startDate, endDate) => {        
    if ( currentDate.getTime() > startDate.getTime() && currentDate.getTime() <= endDate.getTime()) {
        return "Ongoing";
    } else if (currentDate.getTime() > startDate.getTime() && currentDate.getTime() > endDate.getTime()) {
        return 'Past'
    } else if (currentDate.getTime() < startDate.getTime()) {
        return 'Upcoming'
    } else if (currentDate.getTime() === startDate.getTime() && currentDate.getTime() === endDate.getTime()) {
        return 'Today'                
    } 
}

const DisplayEventView = () => {
        
    const isViewingEvent = useSelector(state => state.api.isViewingEvent);
    const currentEvent = useSelector(state => state.api.currentEvent);
    const products = useSelector(state => state.api.eventProducts);
    const dispatch = useDispatch();
    
    let view;

    if (isViewingEvent) { 
  
        let imageHero = currentEvent.event_hero_image;
        let imageOrganizer = currentEvent.organizer.image;
        let venue = currentEvent.venue;

        const currentDate = new Date();
        const startDate = createDateFromString(currentEvent.start_time);
        const endDate = createDateFromString(currentEvent.end_time);
        const eventStatus = calculateEventStatus(currentDate, startDate, endDate);        
           
        if(!venue) {
            venue = "Venue not set yet!"
        }
        if(!imageHero) {
            imageHero = 'https://images.unsplash.com/photo-1598209494655-b8e249540dfc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80';
        }
        if(!imageOrganizer){
            imageOrganizer = 'https://images.unsplash.com/photo-1512314889357-e157c22f938d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80';
        }

        const heroImageStyle = {
            backgroundImage: 'url(' + imageHero +')',
            backgroundPosition: 'center',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            color: 'white',
            height: '25vw',
            alignItems: 'center'                             
        }

        const content = {
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
        }

       view = 
                <div className='container'>                    
                    <div className="container justify-content-center" style={heroImageStyle}>                        
                        <div className='container' style={content}>
                            <div className='row' style={{margin:'2vw'}}>                                                  
                            </div> 
                            <div className='row justify-content-end'>
                                <img className="img-fluid rounded" style={{width:'15%', marginRight:'2vw'}} alt="Event Detail Cover" src={imageOrganizer}/>
                            </div>
                            <div className="row">                                
                                <div className="col-sm-1" >
                                </div>
                                <div className="col-sm-3" >
                                    <h3>{currentEvent.name}</h3>
                                </div>    
                                <div className="col-sm">                                
                                </div>                            
                            </div>                        
                    </div>

                    <div className='row justify-content-start' style={{color: 'black', margin:'0.5vw'}}>
                    <div className="col-sm-4">
                            {currentEvent.tags.map((tag, index) => {                                   
                                return <button className='btn btn-secondary' key={index} style={{margin:'0.4vw'}}> {tag} </button>
                            })}               
                    </div>
                        
                        <div className='col-sm-4' style={{textAlign:'center'}} >                               
                            <h4 style={{textAlign:'center', margin:'0.5vw'}}>{venue} </h4>
                            <p >{currentEvent.venue_address}</p>
                            <br></br> 
                            <h5>STATUS</h5>
                            <h3> <span className='badge badge-primary'>{eventStatus}</span> </h3>
                            <p>START DAY</p>
                            <h4>{startDate.getDate()}.{startDate.getMonth()+1}.{startDate.getFullYear()}</h4>
                            <p>END DAY</p>
                            <h4>{endDate.getDate()}.{endDate.getMonth()+1}.{endDate.getFullYear()} </h4>
                            
                        </div>

                        <div className='col-sm-4'>
                            <h5>Available products</h5> 
                            {products.map((product, index) => {
                                 return     <div style={{margin:'0.3vw'}} >
                                             <button onClick={ () => togglePriceVisibility({index})} key={index} className="btn btn-info"> {product.name}  </button>
                                                <div id={index} style={{display: 'none'}}>
                                                {
                                                  product.variants.map((variant) => {
                                                      return <p> {variant.name} {variant.price}€ </p> 
                                                  })
                                                }
                                                </div>
                                             </div>
                            })}
                        </div>
                    </div>
                    <div className='row justify-content-end' style={{color: 'black'}}>                        
                        <div className='col-sm-1' style={{margin: '1.5vw'}} >
                            <button className="btn btn-info" onClick={()=>dispatch(toggleEventView())}> BACK </button>  
                        </div>
                    </div>                   
                </div>
            </div>
            }
    return (view);
}

export const EventView = () => {
        const eventView = DisplayEventView();
        return (
            <div className="container justify-content-center">
                {eventView}
            </div>
        );
    }
