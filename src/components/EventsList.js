import React, { useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchEvents, setIndex, fetchProducts, toggleEventView } from '../slices/apiSlice';
import 'bootstrap/dist/css/bootstrap.min.css';

  const CreateList = () => { 

  const events = useSelector(state => state.api.events);
  const nextEvents = useSelector(state => state.api.nextEvents);
  const previousEvents = useSelector(state => state.api.previousEvents);
  const isViewingEvent = useSelector(state => state.api.isViewingEvent);
  
  const dispatch = useDispatch();

  useEffect(() => {dispatch(fetchEvents("https://api.liveto.io/api/v1/events/?format=json"))}, []);
    
  const viewState = useSelector(state => state.api.isViewingEvent);
  console.log(viewState);
  
  let eventList;            

  const eventArray = events.results;

  if (eventArray && !isViewingEvent) 
    {
      let nextUrl, prevUrl;        
      if(nextEvents) {
          nextUrl =  <button className="btn btn-info" onClick={() => dispatch(fetchEvents(nextEvents))}> NEXT</button> 
      } else {
          nextUrl =  <button className="btn btn-info" onClick={() => dispatch(fetchEvents(nextEvents))} disabled> NEXT</button>
      }
      
      if(previousEvents) {
          prevUrl = <button className="btn btn-info " onClick={() => dispatch(fetchEvents(previousEvents))}>PREV</button>
      } else {
          prevUrl = <button className="btn btn-info " onClick={() => dispatch(fetchEvents(previousEvents))} disabled>PREV</button>
      }
      eventList =
      <div className="container justify-content-center">        
          <table className="table">
              <tbody>                    
                  <tr>                      
                    <th> <h3>Events</h3>{prevUrl}<span>&nbsp;&nbsp;</span>{nextUrl}</th>
                  </tr>
                  {
                    eventArray.map((event, index) => {
                          let imageMain = event.event_main_image;                            
                        if(!imageMain) {
                            imageMain = "https://images.unsplash.com/photo-1511578314322-379afb476865?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80";
                        }                         
                          return <tr key={index}>
                                      <td><img className="img-fluid rounded"  src={imageMain} alt="Event Info Cover"/></td>                                                                                
                                      <td>
                                          <div className="row">
                                              <h3>{event.name}</h3>
                                          </div>
                                          <div className="row"> 
                                              <p>{event.organizer.name}</p>                                             
                                          </div>
                                          <div className="row">
                                              <button className="btn btn-info" onClick={() => {dispatch(setIndex({index})); dispatch(fetchProducts(events.results[index].slug))}}>Read More </button>
                                          </div>
                                      </td>
                                  </tr>
                      })
                  }
                  <tr><th>{prevUrl} <span>&nbsp;&nbsp;</span> {nextUrl}</th></tr>
              </tbody>                
          </table>                     
      </div>       
    }
    
      return(eventList);   
}

export const EventsList = () => {   
  return CreateList();
}
