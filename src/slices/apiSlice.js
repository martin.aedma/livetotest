import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'

const initialState = {
    isViewingEvent : false,
    events: [],
    nextEvents: null,
    previousEvents: null,
    loading: false,
    error: null,
    eventIndex : null,
    currentEvent : null,
    eventProducts : [],
    eventStartDate : null,
    eventEndDate : null,
};

export const fetchEvents = createAsyncThunk('api/fetchEvents', async(url) => {  
    try {        
        const response = await fetch( url, {
            method : 'get',
            headers: {'Content-Type':'application/json','Accept': 'application/json'}
        })
        .then (function(response) {
            return response.json();
        })

        const data = await response;
        return data;
    }
    catch (error) {
        throw Error(error);
    }
});

export const fetchProducts = createAsyncThunk('api/fetchProducts', async(slug) => {
    try {
        const query = 'https://api.liveto.io/api/v1/events/' + slug + '/products/?format=json';
        const response = await fetch(query, {
            method: 'get',
            headers: {'Content-Type':'application/json','Accept': 'application/json'}    
        })
        .then(function(response) {
            return response.json();
        })

        const data = await response;
        return data;
    } 
    catch (error) {
        throw Error(error);
    }
})

const api = createSlice({
    name : "api",
    initialState,
    reducers : {
        toggleEventView : {
            reducer : (state, action) => {
                state.isViewingEvent = !state.isViewingEvent
            }
        },
        setIndex(state, action) {
            state.eventIndex = action.payload
        },
    },
    extraReducers : {
        [fetchEvents.pending] : (state, action) => {
            state.loading = true;
            state.error = null;
        },
        [fetchEvents.fulfilled] : (state, action) => {
            state.events = action.payload;
            state.nextEvents = action.payload.next;
            state.previousEvents = action.payload.previous;            
            state.loading = false;
        },
        [fetchEvents.rejected] : (state, action) => {
            state.error = action.error.message;
            state.loading = false;
        },
        [fetchProducts.pending] : (state, action) => {
            state.loading = true;
            state.error = null;
        },
        [fetchProducts.fulfilled] : (state, action) => {
            state.isViewingEvent = !state.isViewingEvent;
            state.eventProducts = action.payload;
            state.currentEvent = state.events.results[state.eventIndex.index];
            state.eventStartDate = state.events.results[state.eventIndex.index].start_time;
            state.eventEndDate = state.events.results[state.eventIndex.index].end_time;
            state.loading = false;
        },
        [fetchProducts.rejected] : (state, action) => {
            state.error = action.error.message;
            state.loading = false;
        },
                
    }
});

export const { setIndex, toggleEventView} = api.actions;
export default api.reducer;
